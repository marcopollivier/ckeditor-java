<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page language="Java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Creating CKEditor Instances &mdash; CKEditor Sample</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type"/>
	<link type="text/css" rel="stylesheet" href="ckeditor/_samples/sample.css" />
</head>
<body>
	
	<a href="replace.jsp">Replace</a><br/>
	<a href="replace_all.jsp">Replace All</a><br/>
	<a href="standalone.jsp">Standalone</a><br/>
	<a href="advanced.jsp">Advanced</a><br/>
	
</body>
</html>
